#!/usr/bin/env bash

CWD=$(cd . && pwd)
PRG=$0
BASEDIR=$(cd "$(dirname $PRG)" && pwd)
BASEAPP=$(basename "$PRG")
while [ -L "$BASEDIR/$BASEAPP" ]; do
  PRG=$(readlink "$BASEDIR/$BASEAPP")
  BASEDIR=$(cd "$BASEDIR" && cd "$(dirname $PRG)" && pwd)
  BASEAPP=$(basename "$PRG")
done

DATADIR="$BASEDIR/zk"
if [ -d "$DATADIR" ]; then
  echo "$DATADIR already exist"
else
  mkdir -p "$DATADIR" \
  && chown 0.0 "$DATADIR" \
  && chmod 755 "$DATADIR" \
  && echo "$DATADIR created"
fi

for a in 1 2 3; do
  DATADIR="$BASEDIR/zk$a"
  if [ -d "$DATADIR" ]; then
    echo "$DATADIR already exist"
  else
    mkdir -p "$DATADIR" \
    && chown 0.0 "$DATADIR" \
    && chmod 755 "$DATADIR" \
    && echo "$DATADIR created"
  fi
done

