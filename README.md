# ZooKeeper container
This is a sample application deployment using [docker compose](https://docs.docker.com/compose/).

### Application list
* [ZooKeeper](https://zookeeper.apache.org)
* [ZooNavigator](https://github.com/elkozmon/zoonavigator)

## Getting Started

### Prerequisites
* [Docker](https://www.docker.com)
* [Docker Compose](https://docs.docker.com/compose/)

### Running the container
go to the checkout directory and follow these steps
* Rename docker-compose.yml.example to docker-compose.yml
* Initialize container directory
```
./init.sh
```
* Starting the application container
```
docker-compose up -d
```

### Shutdown
Run the command in the checkout directory
```
docker-compose down -v
```
